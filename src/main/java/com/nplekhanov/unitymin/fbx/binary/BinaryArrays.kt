package com.nplekhanov.unitymin.fbx.binary

import com.nplekhanov.unitymin.fbx.model.PropertyType
import com.twelvemonkeys.io.LittleEndianDataInputStream
import com.twelvemonkeys.io.LittleEndianDataOutputStream
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

object BinaryArrays {


    fun <A> arrayType(
            type: PropertyType,
            stride: Int,
            readElement: (reader: Reader) -> A,
            writeElement: (writer: Writer, element: A) -> Unit
    ): BinaryPropertyType<List<A>> =
            BinaryPropertyType(type,
                    { decoder ->
                        val elementsCount = decoder.readUnsignedInt().toInt()
                        val encoding = decoder.readUnsignedInt().toInt()
                        val bytesCount = decoder.readUnsignedInt().toInt()
                        var bytes = decoder.readBytes(bytesCount)

                        val decompress = encoding == 1
                        if (decompress) {
                            bytes = Zlib.decompress(bytes)
                        }
                        var list = Reader(
                                LittleEndianDataInputStream(ByteArrayInputStream(bytes)),
                                bytes.size.toLong())
                                .let { bd ->
                                    (0 until elementsCount)
                                            .map { readElement(bd) }
                                }
                        if (decompress) {
                            list = DecompressedArrayList(list)
                        }
                        list
                    },
                    { encoder, value ->
                        val arrayLength = value.size.toLong()
                        var encoded = ByteArrayOutputStream().let { buffer ->
                            val bufferEncoder = Writer(LittleEndianDataOutputStream(buffer))
                            for (element in value) {
                                writeElement(bufferEncoder, element)
                            }
                            buffer.toByteArray()
                        }

                        val compress = value is DecompressedArrayList
                        if (compress) {
                            encoded = Zlib.compress(encoded)
                        }

                        encoder.writeUnsignedInt(arrayLength)
                        encoder.writeUnsignedInt(if (compress) 1 else 0)
                        encoder.writeUnsignedInt(encoded.size.toLong())
                        encoder.writeBytes(encoded)
                    })
}