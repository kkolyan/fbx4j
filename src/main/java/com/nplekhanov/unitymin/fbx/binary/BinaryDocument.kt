package com.nplekhanov.unitymin.fbx.binary

import com.nplekhanov.unitymin.fbx.model.Document
import com.nplekhanov.unitymin.fbx.model.Node
import com.nplekhanov.unitymin.fbx.model.children
import com.twelvemonkeys.io.LittleEndianDataInputStream
import com.twelvemonkeys.io.LittleEndianDataOutputStream
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets
import kotlin.experimental.xor

object BinaryDocument {
    fun readDocument(stream: InputStream, length: Long): Document {

        val reader = Reader(LittleEndianDataInputStream(BufferedInputStream(stream)), length)

        reader.readAscii(formatMark.length).also {
            if (it != formatMark) {
                throw IllegalStateException("wrong file format")
            }
        }
        val version = reader.readUnsignedInt()
        if (version != 7100L) {
            throw IllegalStateException()
        }
        val content = arrayListOf<Node>()
        while (true) {
            val node = BinaryNode.readNode(reader) ?: break
            content += node
        }

        return Document(version, content)
    }

    fun readDocument(file: File): Document {
        return FileInputStream(file)
                .use { readDocument(it, file.length()) }
    }

    fun writeDocument(document: Document, stream: OutputStream) {

        val writer = Writer(LittleEndianDataOutputStream(stream), 0)

        writer.writeBytes(formatMark.toByteArray(StandardCharsets.US_ASCII))
        writer.writeUnsignedInt(document.version)
        for (node in document.content) {
            BinaryNode.writeTo(node, writer)
        }
        BinaryNode.writeTo(null, writer)

        writer.writeBytes(generateFooter(document.content))
        writer.writeBytes(ByteArray(20))
        writer.writeInt(document.version.toInt())
        writer.writeBytes(ByteArray(120))
    }

    fun writeDocument(document: Document, file: File) {
        FileOutputStream(file)
                .use {
                    writeDocument(document, it)
                    it.flush()
                }
    }

    const val formatMark = "Kaydara FBX Binary  ${0x00.toChar()}${0x1A.toChar()}${0x00.toChar()}"
    private fun generateFooter(content: List<Node>): ByteArray {
        val timeStamp = content[0].children("CreationTimeStamp").single()
        val ts = timeStamp.children!!.slice(0..7).map { it.properties.single().value as Int }
        val version = ts[0]
        val year = ts[1]
        val month = ts[2]
        val day = ts[3]
        val hour = ts[4]
        val minute = ts[5]
        val second = ts[6]
        val millisecond = ts[7]

        val mangledTime = String.format("%02d%02d%02d%02d%02d%04d%02d",
                second, month, hour, day, (millisecond / 10), year, minute)
        val magledBytes = mangledTime.toByteArray(StandardCharsets.US_ASCII)

        val sourceId = arrayOf(0x58, 0xAB, 0xA9, 0xF0, 0x6C, 0xA2, 0xD8, 0x3F, 0x4D, 0x47, 0x49, 0xA3, 0xB4, 0xB2, 0xE7, 0x3D)
                .map { it.toByte() }.toByteArray()

        val key = arrayOf(0xE2, 0x4F, 0x7B, 0x5F, 0xCD, 0xE4, 0xC8, 0x6D, 0xDB, 0xD8, 0xFB, 0xD7, 0x40, 0x58, 0xC6, 0x78)
                .map { it.toByte() }.toByteArray()


        // Turns out this is the algorithm they use to generate the footer. Who knew!
        fun encrypt(a: ByteArray, b: ByteArray) {
            val footerCodeSize = 16
            var c: Byte = 64
            for (i in 0 until footerCodeSize) {
                a[i] = (a[i] xor (c xor b[i]))
                c = a[i]
            }
        }
        encrypt(sourceId, magledBytes)
        encrypt(sourceId, key)
        encrypt(sourceId, magledBytes)
        return sourceId
    }
}