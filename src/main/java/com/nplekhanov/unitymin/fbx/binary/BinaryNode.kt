package com.nplekhanov.unitymin.fbx.binary

import com.nplekhanov.unitymin.fbx.model.Property
import com.nplekhanov.unitymin.fbx.model.Node
import com.twelvemonkeys.io.LittleEndianDataOutputStream
import java.io.ByteArrayOutputStream

object BinaryNode {
    fun readNode(reader: Reader): Node? {
        val endOffset = reader.readUnsignedInt()
        if (endOffset < 0) {
            throw IllegalStateException("invalid format")
        }
        val numProperties = reader.readUnsignedInt()
        val propertyListLen = reader.readUnsignedInt()
        val name = reader.readUBytePrefixedText()
        val properties = readProperties(reader, numProperties)
        val nestedList = readNestedList(reader, endOffset)
        if (endOffset == 0L) {
            return null
        }
        return Node(
                numProperties,
                propertyListLen,
                name,
                properties,
                nestedList
        )
    }

    private fun readProperties(reader: Reader, numProperties: Long): List<Property> {
        return (0 until numProperties).map {
            BinaryProperty.readProperty(reader)
        }
    }

    private fun readNestedList(reader: Reader, endOffset: Long): List<Node>? {
        if (endOffset == 0L) {
            return null
        }
        if (reader.position < endOffset) {
            val list = mutableListOf<Node>()
            while (true) {
                val node = readNode(reader) ?: break
                list += node
            }
            if (reader.position != endOffset) {
                throw IllegalStateException("invalid format")
            }
            return list
        }
        return null
    }

    fun writeTo(node: Node?, writer: Writer) {
        Ndc.with(node?.name ?: "<null>") {
            val buffer = createBodyBuffer(node, writer.written)
            if (node == null) {
                writer.writeUnsignedInt(0)
            } else {
                writer.writeUnsignedInt(writer.written + 4 + buffer.size.toLong())
            }
            writer.writeBytes(buffer)
        }
    }

    private fun createBodyBuffer(node: Node?, alreadyWritten: Long): ByteArray {
        val buffer = ByteArrayOutputStream()
        val writtenIntFutureEndOffset = 4L
        val bufferWriter = Writer(
                LittleEndianDataOutputStream(buffer),
                alreadyWritten + writtenIntFutureEndOffset
        )
        bufferWriter.writeUnsignedInt(node?.numProperties ?: 0)
        bufferWriter.writeUnsignedInt(node?.propertyListLen ?: 0)
        bufferWriter.writeUBytePrefixedText(node?.name ?: "")

        node?.properties?.forEach { property ->
            BinaryProperty.writeProperty(bufferWriter, property)
        }
        node?.children?.forEach { child ->
            writeTo(child, bufferWriter)
        }
        if (node?.children != null) {
            writeTo(null, bufferWriter)
        }
        //+4 to compensate offset itself
        if (alreadyWritten + buffer.size().toLong() != bufferWriter.written - writtenIntFutureEndOffset) {
            throw IllegalStateException()
        }
        return buffer.toByteArray()
    }
}