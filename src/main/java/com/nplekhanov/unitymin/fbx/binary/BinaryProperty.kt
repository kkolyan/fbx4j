package com.nplekhanov.unitymin.fbx.binary

import com.nplekhanov.unitymin.fbx.model.Property
import com.nplekhanov.unitymin.fbx.model.PropertyType
import java.nio.charset.StandardCharsets

object BinaryProperty {
    fun readProperty(reader: Reader): Property {
        val typeCode = reader.readAscii(1)

        val type = PropertyType.byCode[typeCode] ?: throw IllegalStateException("unknown type code: $typeCode")
        val binaryType = BinaryPropertyTypes.resolveType(type)
        val value = binaryType.decode(reader)
        return Property(type, value, value is DecompressedArrayList<*>)
    }

    fun writeProperty(writer: Writer, property: Property) {
        writer.writeBytes(property.type.code.toByteArray(StandardCharsets.US_ASCII))
        val binaryType = BinaryPropertyTypes.resolveType(property.type)
        var value = property.value
        if (property.decompressed && value !is DecompressedArrayList<*>) {
            value = DecompressedArrayList(value as List<Any>)
        }
        binaryType.encode(writer, value)
    }
}