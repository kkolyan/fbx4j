package com.nplekhanov.unitymin.fbx.binary

import com.nplekhanov.unitymin.fbx.model.PropertyType

class BinaryPropertyType<T>(
        val type: PropertyType,
        val decode: (reader: Reader) -> T,
        val encode: (writer: Writer, value: T) -> Unit
)