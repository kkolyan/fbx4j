package com.nplekhanov.unitymin.fbx.binary

import com.nplekhanov.unitymin.fbx.model.PropertyType
import java.nio.charset.StandardCharsets

object BinaryPropertyTypes {

    fun resolveType(type: PropertyType): BinaryPropertyType<Any> {
        @Suppress("UNCHECKED_CAST")
        return types[type] as BinaryPropertyType<Any>?
                ?: throw IllegalStateException("unknown type: $type")
    }

    private val types = listOf(
            BinaryPropertyType(PropertyType.Short,
                    { decoder -> decoder.readShort() },
                    { encoder, value -> encoder.writeShort(value) }),

            BinaryPropertyType(PropertyType.Byte,
                    { decoder -> decoder.readUnsignedByte()/* not always 0|1 values*/ },
                    { encoder, value -> encoder.writeUnsignedByte(value) }),

            BinaryPropertyType(PropertyType.Int,
                    { decoder -> decoder.readInt() },
                    { encoder, value -> encoder.writeInt(value) }),

            BinaryPropertyType(PropertyType.Float,
                    { decoder -> decoder.readFloat() },
                    { encoder, value -> encoder.writeFloat(value) }),

            BinaryPropertyType(PropertyType.Double,
                    { decoder -> decoder.readDouble() },
                    { encoder, value -> encoder.writeDouble(value) }),

            BinaryPropertyType(PropertyType.Long,
                    { decoder -> decoder.readLong() },
                    { encoder, value -> encoder.writeLong(value) }),

            BinaryArrays.arrayType(PropertyType.ListOfFloat, 4,
                    { decoder -> decoder.readFloat() },
                    { encoder, it -> encoder.writeFloat(it) }),

            BinaryArrays.arrayType(PropertyType.ListOfDouble, 8,
                    { decoder -> decoder.readDouble() },
                    { encoder, it -> encoder.writeDouble(it) }),

            BinaryArrays.arrayType(PropertyType.ListOfLong, 8,
                    { decoder -> decoder.readLong() },
                    { encoder, it -> encoder.writeLong(it) }),

            BinaryArrays.arrayType(PropertyType.ListOfInt, 4,
                    { decoder -> decoder.readInt() },
                    { encoder, it -> encoder.writeInt(it) }),

            BinaryArrays.arrayType(PropertyType.ListOfBoolean, 1,
                    { decoder -> decoder.readUnsignedByte() == 0x01 },
                    { encoder, it -> encoder.writeUnsignedByte(if (it) 0x01 else 0x00) }),

            BinaryPropertyType(PropertyType.String,
                    { decoder -> decoder.readAscii(decoder.readUnsignedInt().toInt()) },
                    { encoder, value ->
                        val bytes = value.toByteArray(StandardCharsets.US_ASCII)
                        encoder.writeUnsignedInt(bytes.size.toLong())
                        encoder.writeBytes(bytes)
                    }),

            BinaryPropertyType(PropertyType.Raw,
                    { decoder -> decoder.readBytes(decoder.readUnsignedInt().toInt()) },
                    { encoder, value ->
                        encoder.writeUnsignedInt(value.size.toLong())
                        encoder.writeBytes(value)
                    })

    ).associateBy { it.type }
}