package com.nplekhanov.unitymin.fbx.binary

class DecompressedArrayList<T>(source: Iterable<T>) : ArrayList<T>() {
    init {
        for (element in source) {
            add(element)
        }
    }
}