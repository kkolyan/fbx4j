package com.nplekhanov.unitymin.fbx.binary

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.mutable.MutableLong
import java.util.ArrayDeque

object Ndc {
    private val stack = ArrayDeque<String>()
    private val counters = mutableMapOf<String, MutableLong>()

    fun dive(name: String) {
        val indent = StringUtils.repeat("  ", stack.size)
        println("$indent$name")
        stack.push(name)
    }

    fun getPath(): String {
        return stack.reversed().joinToString("/")
    }

    fun getPathId(): String {
        val path = getPath()
        return path + "/" + counters[path]
    }

    fun emerge() {
        stack.pop()
    }

    fun <T> with(name: String, actions: () -> T): T {
        dive(name)
        counters.computeIfAbsent(getPath()) { MutableLong() }.increment()
        val result = actions()
        emerge()
        return result
    }
}