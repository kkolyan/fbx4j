package com.nplekhanov.unitymin.fbx.binary

import com.twelvemonkeys.io.LittleEndianDataInputStream
import java.io.BufferedInputStream
import java.io.ByteArrayInputStream
import java.io.DataInput
import java.io.File
import java.io.InputStream
import java.nio.charset.StandardCharsets

class Reader(private val input: DataInput, private val length: Long) {
    var position: Long = 0
        private set

    companion object {
        fun of(file: File) : Reader {
            val bytes = file.readBytes()
            return of(ByteArrayInputStream(bytes), bytes.size.toLong())
        }
        fun of(input: InputStream, length: Long) : Reader {
            return Reader(LittleEndianDataInputStream(BufferedInputStream(input)), length)
        }
    }

    private fun forward(bytes: Int) {
        if (bytes < 0) {
            throw IllegalStateException()
        }
        position += bytes
    }

    fun readUnsignedInt(): Long {
        forward(4)
        val d = input.readUnsignedByte().toLong()
        val c = input.readUnsignedByte().toLong()
        val b = input.readUnsignedByte().toLong()
        val a = input.readUnsignedByte().toLong()
        return (a shl 24) + (b shl 16) + (c shl 8) + (d shl 0)
    }

    fun readUBytePrefixedText(): String {
        val length = readUnsignedByte()
        return readAscii(length)
    }

    fun readAscii(length: Int): String {
        return readBytes(length).toString(StandardCharsets.US_ASCII)
    }

    fun readUnsignedByte(): Int {
        forward(1)
        return input.readUnsignedByte()
    }

    fun readShort(): Short {
        forward(2)
        return input.readShort()
    }

    fun readInt(): Int {
        forward(4)
        return input.readInt()
    }

    fun readFloat(): Float {
        forward(4)
        return input.readFloat()
    }

    fun readDouble(): Double {
        forward(8)
        return input.readDouble()
    }

    fun readLong(): Long {
        forward(8)
        return input.readLong()
    }

    fun readBytes(bytes: Int): ByteArray {
        forward(bytes)
        return ByteArray(bytes).also {
            input.readFully(it)
        }
    }

    fun readRemainder(): ByteArray {
        val buffer = ByteArray((length - position).toInt())
        input.readFully(buffer)
        forward(buffer.size)
        return buffer
    }
}