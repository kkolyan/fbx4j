package com.nplekhanov.unitymin.fbx.binary

import java.io.DataOutput
import java.nio.charset.StandardCharsets

class Writer(
        private val output: DataOutput,
        alreadyWritten: Long? = null
) {

    val countBytes = alreadyWritten != null
    var written: Long = alreadyWritten ?: -1L
        private set

    fun writeUnsignedInt(v: Long) {
        output.writeByte((0xffL and v).toInt())
        output.writeByte((0xffL and (v shr 8)).toInt())
        output.writeByte((0xffL and (v shr 16)).toInt())
        output.writeByte((0xffL and (v shr 24)).toInt())
        forward(4)
    }

    fun writeUnsignedByte(value: Int) {
        output.writeByte(value)
        forward(1)
    }

    fun writeShort(value: Short) {
        output.writeShort(value.toInt())
        forward(2)
    }

    fun writeInt(value: Int) {
        output.writeInt(value)
        forward(4)
    }

    fun writeFloat(value: Float) {
        output.writeFloat(value)
        forward(4)
    }

    fun writeDouble(value: Double) {
        output.writeDouble(value)
        forward(8)
    }

    fun writeLong(value: Long) {
        output.writeLong(value)
        forward(8)
    }

    fun writeBytes(value: ByteArray) {
        output.write(value)
        forward(value.size)
    }

    private fun forward(bytes: Int) {
        val checkPoint = 2387225
        if (countBytes && written <= checkPoint && checkPoint < written + bytes) {
            System.out.flush()
            println("difference here:")
        }
        written += bytes
    }

    fun writeUBytePrefixedText(name: String) {
        val nameBytes = name.toByteArray(StandardCharsets.US_ASCII)
        writeUnsignedByte(nameBytes.size)
        writeBytes(nameBytes)
    }
}
