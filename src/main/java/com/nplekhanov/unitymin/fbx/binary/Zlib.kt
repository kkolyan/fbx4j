package com.nplekhanov.unitymin.fbx.binary

import org.apache.commons.io.IOUtils
import java.io.ByteArrayInputStream
import java.util.zip.Deflater
import java.util.zip.DeflaterInputStream
import java.util.zip.InflaterInputStream

object Zlib {
    fun compress(byteArray: ByteArray): ByteArray {
        return IOUtils.toByteArray(DeflaterInputStream(ByteArrayInputStream(byteArray), Deflater(Deflater.BEST_SPEED)))
    }

    fun decompress(byteArray: ByteArray): ByteArray {
        return IOUtils.toByteArray(InflaterInputStream(ByteArrayInputStream(byteArray)))
    }
}