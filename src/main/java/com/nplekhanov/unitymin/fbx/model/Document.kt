package com.nplekhanov.unitymin.fbx.model

class Document(
        val version: Long,
        val content: List<Node>
)