package com.nplekhanov.unitymin.fbx.model


class Node(
        val numProperties: Long,
        val propertyListLen: Long,
        val name: String,
        val properties: List<Property>,
        val children: List<Node>?
) {

    override fun toString(): String {
        return name
    }
}