package com.nplekhanov.unitymin.fbx.model


class Property(
        val type: PropertyType,
        value: Any,
        val decompressed: Boolean
) {

    var value = value
        private set

    fun <T> open(): TypedProperty<T> {
        return object : TypedProperty<T> {
            override var value: T
                get() = this@Property.value as T
                set(v) {
                    this@Property.value = v!!
                }

            override fun toString(): String {
                return this@Property.toString()
            }
        }
    }

    override fun toString(): String {
        return "${type.code}: $value"
    }

}