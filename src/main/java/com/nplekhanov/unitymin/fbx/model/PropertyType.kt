package com.nplekhanov.unitymin.fbx.model

enum class PropertyType(val code: kotlin.String) {

    Short("Y"),

    /**
     * here (https://code.blender.org/2013/08/fbx-binary-file-format-specification/)
     * said that it is 0 or 1, but I encountered 84. Typed as Int 0..255
     */
    Byte("C"),

    Int("I"),
    Float("F"),
    Double("D"),
    Long("L"),

    ListOfFloat("f"),
    ListOfDouble("d"),
    ListOfLong("l"),
    ListOfInt("i"),
    ListOfBoolean("b"),

    String("S"),
    Raw("R");


    companion object {
        val byCode = values().associateBy { it.code }
    }
}