package com.nplekhanov.unitymin.fbx.model

interface TypedProperty<T> {
    var value : T
}