package com.nplekhanov.unitymin.fbx.model

fun Node.children(nestedName: String): List<Node> {
    return children!!.filter { it.name == nestedName }
}

fun <T> Node.prop(index: Int) : TypedProperty<T> {
    return properties[index].open()
}

fun Node.child(name: String) : Node {
    return children(name).single()
}
fun Document.child(name: String) : Node {
    return children(name).single()
}

fun Document.children(nestedName: String): List<Node> {
    return content!!.filter { it.name == nestedName }
}