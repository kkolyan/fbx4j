package com.nplekhanov.unitymin.fbx.test

import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream

fun main(args: Array<String>) {
    val fname = "Adam_Knight.FBX"
    val offset = getFirstDifferenceOffset(
            File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\$fname"),
            File("C:\\dev\\on-foot-models\\Minified\\$fname")
    )
    if (offset != null) {
        println("difference offset: $offset")
    }
}

fun getFirstDifferenceOffset(a: File, b: File): Int? {
    val differences = mutableListOf<Int>()

    BufferedInputStream(FileInputStream(a)).use { a1 ->
        BufferedInputStream(FileInputStream(b)).use { b1 ->
            var i = 0
            while (true) {
                val readA = a1.read()
                val readB = b1.read()
                if (readA < 0 && readB < 0) {
                    break
                }
                if (readA != readB) {
                    differences.add(i)
                    if (differences.size > 4) {
                        val last4ByteOffsets = ((differences.size - 4)..(differences.size - 1))
                        val sequential = last4ByteOffsets.map { j -> differences[j] - differences[j - 1] == 1 }
                        if (sequential.all { it }) {
                            return i
                        }
                    }
                    println("$i: $readA != $readB")
//                    return i
                }
                i += 1
            }
        }
    }
    return null
}