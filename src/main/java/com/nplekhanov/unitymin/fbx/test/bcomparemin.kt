package com.nplekhanov.unitymin.fbx.test

import java.io.File

fun main(args: Array<String>) {
    val path = "C:\\dev\\on-foot-models\\Minified"
    val firstDifferenceOffset = getFirstDifferenceOffset(
            File("$path/Adam_Knight.FBX"),
            File("$path/Adam_Knight.FBX.reencoded2.fbx")
    )
    println(firstDifferenceOffset)
}