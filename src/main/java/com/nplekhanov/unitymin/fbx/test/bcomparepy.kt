package com.nplekhanov.unitymin.fbx.test

import java.io.File

fun main(args: Array<String>) {
    val path = "C:\\dev\\on-foot-models\\AdamKnight\\Models"
    val firstDifferenceOffset = getFirstDifferenceOffset(
            File("$path/Adam_Knight.FBX"),
            File("$path/Adam_Knight_out.fbx")
    )
    println(firstDifferenceOffset)
}