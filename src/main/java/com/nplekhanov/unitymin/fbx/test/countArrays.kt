package com.nplekhanov.unitymin.fbx.test

import com.nplekhanov.unitymin.fbx.binary.BinaryDocument
import com.nplekhanov.unitymin.fbx.model.Node
import java.io.File
import java.util.LongSummaryStatistics

fun main(args: Array<String>) {

    val fname = "Adam_Knight.FBX"
    val src = File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\$fname")
    val document = BinaryDocument.readDocument(src)
    val stats = mutableMapOf<String, LongSummaryStatistics>()
    fun traverse(path: String, nodes: List<Node>) {
        nodes.forEach { node ->
            if (node.children == null || node.children.isEmpty()) {
                node.properties.forEachIndexed { i, prop ->
                    val value = prop.value
                    if (value is List<*>) {
                        stats.computeIfAbsent("$path[$i]") { LongSummaryStatistics() }
                                .accept(value.size)
                    }
                }
            } else {
                traverse(path + "/" + node.name, node.children)
            }
        }
    }
    traverse("", document.content)
    stats.toSortedMap().forEach { k, v ->
        println("$k: $v")
    }
}