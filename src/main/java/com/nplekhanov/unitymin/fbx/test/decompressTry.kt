package com.nplekhanov.unitymin.fbx.test

import com.nplekhanov.unitymin.fbx.binary.Zlib
import java.io.File
import java.nio.ByteBuffer

fun parseDouble(source: ByteArray, offset: Int) : Double {
    val chunk = source.sliceArray(offset until (offset + 8))
    chunk.reverse()
    val buffer = ByteBuffer.wrap(chunk)
    return buffer.double
}

fun main(args: Array<String>) {
    val compressed = File("C:\\dev\\unitymin\\temp.bin").readBytes()
    val decompressed = Zlib.decompress(compressed)

    if (decompressed.size % 8 != 0) {
        throw IllegalStateException()
    }

    val array = (0 until decompressed.size step 8).map { parseDouble(decompressed, it) }.toList()
    println(array)
}