package com.nplekhanov.unitymin.fbx.test

import com.nplekhanov.unitymin.fbx.binary.BinaryDocument
import com.nplekhanov.unitymin.fbx.model.Node
import org.apache.commons.lang3.StringUtils
import java.io.File

fun main(args: Array<String>) {
    val doc = BinaryDocument.readDocument(File(
            "C:\\dev\\on-foot-models\\AdamKnight\\Models\\Adam_Knight.FBX"
    ))
    val s = StringBuilder()
    doc.content.toJson(s)
    File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\Adam_Knight.FBX.my.json").writeText(s.toString())
}

val hex = "0123456789abcdef"

fun bytesToHex(bytes: ByteArray): String {
    val hexChars = CharArray(bytes.size * 2)
    for (j in bytes.indices) {
        val v = bytes[j].toInt() and 0xFF
        hexChars[j * 2] = hex[v.ushr(4)]
        hexChars[j * 2 + 1] = hex[v and 0x0F]
    }
    return String(hexChars)
}

fun valueToJson(value: Any): String {

    return when (value) {
        is String -> value.toString()
                .replace("\\", "\\\\")
                .replace("\\p{C}".toRegex(), ":")
                .let { "\"$it\"" }
        is Double -> doubleToText(value)
        is Number -> value.toString()
        is ByteArray -> value
                .map {
                    if (it in 32..126) {
                        it.toChar().toString()
                    } else {
                        val v = it.toInt() and 0xFF
                        "\\\\x${hex[v.ushr(4)]}${hex[v and 0x0F]}"
                    }
                }
                .joinToString("")
                .let { "\"$it\"" }
        is List<*> -> value.map {
            when (it) {
                true -> 1
                false -> 0
                is Double -> {
                    doubleToText(it)
                }
                else -> it
            }
        }.toString()
        else -> throw IllegalStateException("unsupported type: ${value.javaClass}")
    }
}

private fun doubleToText(it: Double): String {
    val s = it.toString().split("E-")
    return if (s.size > 1) {
        "${s[0]}e-${StringUtils.leftPad(s[1], 2, "0")}"
    } else {
        s[0]
    }
}

fun List<Node>.toJson(s: StringBuilder, indent: String = "") {
    s.append("[")
    val nextIndent = "    $indent"
    var firstElement = true
    for (node in this) {
        if (!firstElement) {
            s.append(",")
        } else {
            firstElement = false
        }
        val propValues = node.properties
                .map {
                    valueToJson(it.value)
                }
                .joinToString(", ")
                .let { "[$it]" }
        val propTypes = node.properties.map { it.type.code }
                .joinToString("")
                .let { "\"$it\"" }
        s.append("\n").append(nextIndent).append("[\"").append(node.name).append("\", ").append(propValues).append(", ").append(propTypes).append(", ")
        node.children?.toJson(s, nextIndent)
        s.append("]")
    }
    s.append("]")
}