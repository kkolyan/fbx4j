package com.nplekhanov.unitymin.fbx.test

import com.nplekhanov.unitymin.fbx.binary.BinaryDocument
import java.io.File


fun main(args: Array<String>) {
    val doc = BinaryDocument.readDocument(File(
            "C:\\dev\\on-foot-models\\Minified\\Adam_Knight.FBX"
    ))
    val s = StringBuilder()
    doc.content.toJson(s)
    File("C:\\dev\\on-foot-models\\Minified\\Adam_Knight.FBX.my.json").writeText(s.toString())
}