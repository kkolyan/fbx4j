package com.nplekhanov.unitymin.fbx.test

import org.apache.commons.io.FileUtils
import java.io.File
import java.nio.ByteBuffer

fun main(args: Array<String>) {
    val fname = "Adam_Knight.FBX"
    val bytes = FileUtils.readFileToByteArray(File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\$fname"))
    val m = bytes.sliceArray(27..30)
    val buffer = ByteBuffer.wrap(m)
    val d = m[0].toInt()
    val c = m[1].toInt()
    val b = m[2].toInt()
    val a = m[3].toInt()
    val result = (a shl 24) + (b shl 16) + (c shl 8) + (d shl 0)
    println(result)
    println(buffer.int)
}