package com.nplekhanov.unitymin.fbx.test

import com.nplekhanov.unitymin.fbx.binary.BinaryDocument
import com.nplekhanov.unitymin.fbx.model.Document
import java.io.File

fun main(args: Array<String>) {
    val fname = "Adam_Knight.FBX"
    reencode(
            File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\$fname"),
            File("C:\\dev\\on-foot-models\\Minified\\$fname"),
            true
    )
//    reencode(
//            File("C:\\dev\\on-foot-models\\Minified\\$fname"),
//            File("C:\\dev\\on-foot-models\\Minified\\$fname.reencoded2.fbx")
//    )
}

fun reencode(src: File, dst: File, checkIdentity: Boolean = false, processor: (Document) -> Boolean = {true}) {
    val file = BinaryDocument.readDocument(src)
    if (processor(file)) {
        BinaryDocument.writeDocument(file, dst)
    }

    if (checkIdentity) {
        val differenceOffset = getFirstDifferenceOffset(src, dst)
        if (differenceOffset != null) {
            System.err.println("ERROR: difference at offset $differenceOffset")
        }
    }
}