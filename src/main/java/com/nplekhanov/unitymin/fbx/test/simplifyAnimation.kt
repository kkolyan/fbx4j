package com.nplekhanov.unitymin.fbx.test

import com.nplekhanov.unitymin.fbx.model.TypedProperty
import com.nplekhanov.unitymin.fbx.model.child
import com.nplekhanov.unitymin.fbx.model.prop
import com.nplekhanov.unitymin.fbx.model.children
import java.io.File


fun main(args: Array<String>) {
    val fname = "Adam_Knight.FBX"
    reencode(
            File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\$fname"),
            File("C:\\dev\\on-foot-models\\Minified\\$fname")
    ) { document ->
        document.child("Objects")
                .children("AnimationCurve")
                .forEach { curve ->
                    val keyTime = curve.child("KeyTime").prop<List<Long>>(0)
                    val keyValue = curve.child("KeyValueFloat").prop<List<Float>>(0)
                    val keyCount = curve.child("KeyAttrRefCount").prop<List<Int>>(0)

                    println(keyCount.value)

                    if (keyTime.value.size != keyCount.value.sum() || keyValue.value.size != keyCount.value.sum()) {
                        throw IllegalStateException()
                    }

                    if (keyCount.value.size == 1) {

                        val factor = 10
                        fun <T> strip(value: List<T>): List<T> {
                            return value.reversed()
//                            return value.indices.toList().dropLast(1).map { i -> value[i / factor * factor] } + value.last()
                        }

                        keyTime.value = (keyTime.value)
                        keyValue.value = (keyValue.value).reversed()

//                        strip2(keyCount, keyTime, keyValue)
                    } else if (false) {

                        val factor = 2

                        val segments = mutableListOf<List<Int>>()

                        var lastI = 0
                        for (i in 0 until keyCount.value.size) {
                            segments.add(((lastI until keyCount.value[i]) step factor).toList())
                            lastI = i
                        }

                        keyTime.value = segments.flatten()
                                .map { keyTime.value[it / factor] }
                        keyValue.value = segments.flatten()
                                .map { keyValue.value[it / factor] }
                        keyCount.value = segments.map { it.size }
                    }
                }
        true
    }
}

private fun strip2(keyCount: TypedProperty<List<Int>>, keyTime: TypedProperty<List<Long>>, keyValue: TypedProperty<List<Float>>) {
    val factor = 1

    fun <T> skip(list: List<T>): List<T> {
        return (0 until (keyCount.value.single() - 1) step factor)
                .map { list[it] } + list.last()
    }

    keyTime.value = skip(keyTime.value)
    keyValue.value = skip(keyValue.value)
    keyCount.value = listOf(keyValue.value.size)
}