package com.nplekhanov.unitymin.fbx.test

import java.io.File
import java.nio.charset.StandardCharsets.US_ASCII

fun main(args: Array<String>) {
    val f1 = File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\Adam_Knight.json")
    val f2 = File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\Adam_Knight.FBX.my.json")
    val ignoreLines = setOf(286, 295, 341, 350, 1692)
    val ignorePrefixes = mutableListOf<String>()
    ignorePrefixes += "[\"UV\", "
    ignorePrefixes += "[\"Vertices\", "
    ignorePrefixes += "[\"Normals\", "
    ignorePrefixes += "[\"Shading\", "
    ignorePrefixes += "[\"P\", [\"Lcl Rotation\", \"Lcl Rotation\", \"\", \"A+\","
    ignorePrefixes += "[\"P\", [\"Lcl Translation\", \"Lcl Translation\", \"\", \"A+\","
    ignorePrefixes += "[\"P\", [\"GeometricRotation\", \"Vector3D\", \"Vector\", \"\","
    ignorePrefixes += "[\"Transform\", "
    ignorePrefixes += "[\"TransformLink\", "
    ignorePrefixes += "[\"Weights\", [["
    ignorePrefixes += "[\"P\", [\"d|X\", \"Number\", \"\", \"A+\", "
    ignorePrefixes += "[\"P\", [\"d|Y\", \"Number\", \"\", \"A+\", "
    ignorePrefixes += "[\"P\", [\"d|Z\", \"Number\", \"\", \"A+\", "
    ignorePrefixes += "[\"KeyValueFloat\", [["
    ignorePrefixes += "[\"KeyAttrDataFloat\", [["
    ignorePrefixes += "[\"Default\", "

    compareJson(f1, f2, ignorePrefixes, ignoreLines)
}

fun divDiff(a: Int, b: Int): Double {
    if (a > b) {
        return 1.0 * a / b
    }
    return 1.0 * b / a
}

fun compareJson(f1: File, f2: File, ignorePrefixes: MutableList<String>, ignoreLines: Set<Int>) {
    val t1 = f1.readLines(US_ASCII)
    val t2 = f2.readLines(US_ASCII)
    var i = 0
    fun checkPrefix(line: String?): Boolean {
        return ignorePrefixes.any { line?.trimStart()?.startsWith(it) ?: false }
    }
    while (true) {
        if (i > t1.size && i > t2.size) {
            break
        }
        val l1 = if (t1.size > i) t1[i] else null
        val l2 = if (t2.size > i) t2[i] else null
        if (l1 != l2 && !ignoreLines.contains(i) && !(checkPrefix(l1) && checkPrefix(l2) && divDiff(l1?.length
                        ?: 0, l2?.length ?: 0) < 1.2)) {
            println("difference at line $i:")
            println("1: $l1")
            println("2: $l2")
            break
        }
        i += 1
    }
}