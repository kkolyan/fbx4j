package com.nplekhanov.unitymin.fbx.test

import java.io.File


fun main(args: Array<String>) {
    val f1 = File("C:\\dev\\on-foot-models\\AdamKnight\\Models\\Adam_Knight.FBX.my.json")
    val f2 = File("C:\\dev\\on-foot-models\\Minified\\Adam_Knight.FBX.my.json")
    val ignoreLines = setOf<Int>()
    val ignorePrefixes = mutableListOf<String>()
//    ignorePrefixes += "[\"UV\", "
//    ignorePrefixes += "[\"Vertices\", "
//    ignorePrefixes += "[\"Normals\", "
//    ignorePrefixes += "[\"Shading\", "
//    ignorePrefixes += "[\"P\", [\"Lcl Rotation\", \"Lcl Rotation\", \"\", \"A+\","
//    ignorePrefixes += "[\"P\", [\"Lcl Translation\", \"Lcl Translation\", \"\", \"A+\","
//    ignorePrefixes += "[\"P\", [\"GeometricRotation\", \"Vector3D\", \"Vector\", \"\","
//    ignorePrefixes += "[\"Transform\", "
//    ignorePrefixes += "[\"TransformLink\", "
//    ignorePrefixes += "[\"Weights\", [["
//    ignorePrefixes += "[\"P\", [\"d|X\", \"Number\", \"\", \"A+\", "
//    ignorePrefixes += "[\"P\", [\"d|Y\", \"Number\", \"\", \"A+\", "
//    ignorePrefixes += "[\"P\", [\"d|Z\", \"Number\", \"\", \"A+\", "
//    ignorePrefixes += "[\"KeyValueFloat\", [["
//    ignorePrefixes += "[\"KeyAttrDataFloat\", [["
//    ignorePrefixes += "[\"Default\", "

    compareJson(f1, f2, ignorePrefixes, ignoreLines)
}