package com.nplekhanov.unitymin.fbx.test

import com.nplekhanov.unitymin.fbx.binary.Zlib
import java.io.File

fun main(args: Array<String>) {
    testCompressDecompress(File("a/Objects/AnimationCurve/KeyTime"))
    println("recompressionSuccess: $recompressionSuccess")
    println("recompressionFailure: $recompressionFailure")
    println("redecompressionSuccess: $redecompressionSuccess")
    println("redecompressionFailure: $redecompressionFailure")
}

var recompressionSuccess = 0
var recompressionFailure = 0
var redecompressionSuccess = 0
var redecompressionFailure = 0

fun testCompressDecompress(file: File) {
    if (file.isDirectory) {
        file.listFiles()?.forEach { testCompressDecompress(it) }
    } else if (file.name.endsWith("deflate")) {
        val original = file.readBytes()
        val decompressed = Zlib.decompress(original)
        val recompressed = Zlib.compress(decompressed)
        val redecompressed = Zlib.decompress(recompressed)
        if (original.contentEquals(recompressed)) {
            recompressionSuccess++
        } else {
            recompressionFailure++
        }
        if (decompressed.contentEquals(redecompressed)) {
            redecompressionSuccess++
        } else {
            redecompressionFailure++
        }
        File(file.parent, "${file.name}.recompressed").writeBytes(recompressed)
    }
}
