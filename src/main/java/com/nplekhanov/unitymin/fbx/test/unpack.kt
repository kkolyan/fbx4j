package com.nplekhanov.unitymin.fbx.test

import com.fasterxml.jackson.databind.ObjectMapper
import com.nplekhanov.unitymin.fbx.binary.Zlib
import com.twelvemonkeys.io.LittleEndianDataInputStream
import java.io.ByteArrayInputStream
import java.io.File

fun main(args: Array<String>) {
    unpack(File("a/Objects/AnimationCurve/KeyTime"))
}

fun unpack(file: File) {
    if (file.isDirectory) {
        file.listFiles()?.forEach { unpack(it) }
    } else {
        val original = file.readBytes()
        val decompressed = Zlib.decompress(original)
        val compressed = Zlib.compress(decompressed)
        if (!original.contentEquals(compressed)) {
            throw IllegalStateException()
        }
        val (name, type, stride, ext) = file.name.split(".")
        val input = LittleEndianDataInputStream(ByteArrayInputStream(decompressed))
        val parser: () -> Number  = when (type) {
            "d" -> {{input.readDouble()}}
            "f" -> {{input.readFloat()}}
            "i" -> {{input.readInt()}}
            "l" -> {{input.readLong()}}
            else -> throw IllegalStateException("unknown type: $type")
        }
        val list = (0 until decompressed.size step stride.toInt()).map { parser() }.toList()
        ObjectMapper().writeValue(File(file.parent, "$name.$type.$stride.json"), list)
    }
}
