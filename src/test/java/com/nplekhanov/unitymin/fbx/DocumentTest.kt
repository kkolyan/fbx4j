package com.nplekhanov.unitymin.fbx

import com.nplekhanov.unitymin.fbx.binary.BinaryPropertyType
import com.nplekhanov.unitymin.fbx.binary.BinaryPropertyTypes
import com.nplekhanov.unitymin.fbx.binary.DecompressedArrayList
import com.nplekhanov.unitymin.fbx.binary.Reader
import com.nplekhanov.unitymin.fbx.binary.Writer
import com.nplekhanov.unitymin.fbx.model.PropertyType
import com.twelvemonkeys.io.LittleEndianDataInputStream
import com.twelvemonkeys.io.LittleEndianDataOutputStream
import org.junit.Test
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import kotlin.test.assertEquals


internal class DocumentTest {

    @Test
    fun testUInt() {
        test(1234L, { writer, value -> writer.writeUnsignedInt(value) }, { reader -> reader.readUnsignedInt() })
    }

    @Test
    fun testUByte() {
        test(210, { writer, value -> writer.writeUnsignedByte(value) }, { reader -> reader.readUnsignedByte() })
    }

    @Test
    fun testDouble() {
        test(123153.76, { writer, value -> writer.writeDouble(value) }, { reader -> reader.readDouble() })
    }

    @Test
    fun testDoubleArray() {
        @Suppress("UNCHECKED_CAST")
        val type = BinaryPropertyTypes.resolveType(PropertyType.ListOfDouble)
                as BinaryPropertyType<List<Double>>
        val initialList: List<Double> = DecompressedArrayList(listOf(1.0, 2.0, 3.0, 4.0))
        test(initialList, { writer, list -> type.encode(writer, list) }, { reader -> type.decode(reader) })
    }

    private fun <T> test(v: T, c1: (Writer, T) -> Unit, c2: (Reader) -> T) {
        val buffer = ByteArrayOutputStream()
        val writer = Writer(LittleEndianDataOutputStream(buffer))
        c1(writer, v)
        val reader = Reader(LittleEndianDataInputStream(ByteArrayInputStream(buffer.toByteArray())), buffer.size().toLong())
        assertEquals(v, c2(reader))
    }
}