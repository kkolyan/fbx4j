package com.nplekhanov.unitymin.fbx

import com.nplekhanov.unitymin.fbx.binary.Zlib
import org.junit.Assert.*
import org.junit.Test
import java.nio.charset.StandardCharsets

class ZlibTest {
    @Test
    fun test1() {
        val compressed = Zlib.compress("Hello!".toByteArray(StandardCharsets.US_ASCII))
        val decompressed = Zlib.decompress(compressed)

        assertEquals("Hello!", decompressed.toString(StandardCharsets.US_ASCII))
    }
}